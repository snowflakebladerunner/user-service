package com.hackathon5a.userservice.controller;

import com.hackathon5a.userservice.model.User;
import com.hackathon5a.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService service;

    @GetMapping(value="/get")
    public List<User> getAllUsers() {
        return service.getAllUsers();
    }

    @GetMapping(value = "/{id}")
    public User getUserById(@PathVariable("id") int id) {
        return service.getUser(id);
    }

    @PostMapping(value = "/add")
    public User addUser(@RequestBody User user) {
        return service.newUser(user);
    }

    @PutMapping(value = "/edit")
    public User editUser(@RequestBody User user) {
        return service.saveUser(user);
    }

    @DeleteMapping(value = "/delete/{id}")
    public int deleteUser(@PathVariable int id) {
        return service.deleteUser(id);
    }
}
