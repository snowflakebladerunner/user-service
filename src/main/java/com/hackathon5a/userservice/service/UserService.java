package com.hackathon5a.userservice.service;

import com.hackathon5a.userservice.model.User;
import com.hackathon5a.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    public List<User> getAllUsers() {
        return repository.getAllUsers();
    }

    public User getUser(int id) {
        return repository.getUserById(id);
    }

    public User saveUser(User user) {
        return repository.editUser(user);
    }

    public User newUser(User user) {
        return repository.addUser(user);
    }

    public int deleteUser(int id) {
        return repository.deleteUser(id);
    }
}
