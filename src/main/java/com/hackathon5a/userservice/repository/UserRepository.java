package com.hackathon5a.userservice.repository;

import com.hackathon5a.userservice.model.User;

import java.util.List;

public interface UserRepository {
    public List<User> getAllUsers();

    public User getUserById(int id);

    public User editUser(User user);

    public int deleteUser(int id);

    public User addUser(User user);
}
