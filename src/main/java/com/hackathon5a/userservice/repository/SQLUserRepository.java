package com.hackathon5a.userservice.repository;

import com.hackathon5a.userservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class SQLUserRepository implements UserRepository {

    @Autowired
    JdbcTemplate template;

    @Override
    public List<User> getAllUsers() {
        String sql = "SELECT user_id, user_name, pan, email, phone, cash, equity_value FROM user_details";
        return template.query(sql, new UserRowMapper());
    }

    @Override
    public User getUserById(int id) {
        String sql = "SELECT user_id, user_name, pan, email, phone, cash, equity_value FROM user_details WHERE user_id=?";
        return template.queryForObject(sql, new UserRowMapper(), id);
    }

    @Override
    public User editUser(User user) {
        String sql = "UPDATE user_details SET user_name=?, pan=?, email=?, phone=?, cash=?, equity_value=? WHERE user_id=?";
        template.update(sql, user.getUser_name(), user.getPan(), user.getEmail(), user.getPhone(), user.getCash(), user.getEquity_value(), user.getUser_id());
        return user;
    }

    @Override
    public int deleteUser(int id) {
        String sql = "DELETE FROM user_details WHERE user_id=?";
        return template.update(sql, id);
    }

    @Override
    public User addUser(User user) {
        String sql = "INSERT INTO user_details(user_id, user_name, pan, email, phone, cash, equity_value) VALUES(?,?,?,?,?,?,?)";
        template.update(sql, user.getUser_id(), user.getUser_name(), user.getPan(), user.getEmail(), user.getPhone(), user.getCash(), user.getEquity_value());
        return user;
    }
}

class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new User(
                rs.getInt("user_id"),
                rs.getString("user_name"),
                rs.getString("pan"),
                rs.getString("email"),
                rs.getString("phone"),
                rs.getDouble("cash"),
                rs.getDouble("equity_value")
                        );
    }
}